import java.io.*;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
skahbcjbaskjvbc
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.control.TextArea;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.input.KeyCode;
import javafx.stage.FileChooser;
import java.util.*;
public class Quiz {
    private String nameOfFile;
    private  ArrayList<Question> questions;
    private int correctAnswers = 0;
    private int order = 0;
    public Quiz() {
    	questions = new ArrayList<>();
    }
    public void setName(String nameOfFile) {
        this.nameOfFile = nameOfFile;
    }
    public String getName() {
        return nameOfFile;
    }
    public void addQuestion(Question question) {
        questions.add(question);
    }
    public  Quiz loadFromFile(String nameOffile) throws FileNotFoundException {
            Quiz quiz = new Quiz();    
            File file = new File(nameOffile);       
            Scanner in = new Scanner(file);

            while(in.hasNext()) {
                
            String description = in.nextLine();
            if (description.length() != 0) {
                    
                    if (description.contains("{blank}")) {
                    	FillIn fillInObject = new FillIn();
                    	fillInObject.setDescription(description);
                    	String answerOfQuestion = in.nextLine();
                        
                    	fillInObject.setAnswer(answerOfQuestion);
                    	quiz.addQuestion(fillInObject);
                        
                    }
                         
                    
                    else {
                    	Test testQuestion = new Test();
						testQuestion.setDescription(description);
						String[] options = new String[4];
						for (int i = 0; i < 4; i++) {
							options[i] = in.nextLine();
                            
						}
                        testQuestion.setOptions(options);
						String answerOfQuestion = options[0];
						testQuestion.setAnswer(answerOfQuestion);
						quiz.addQuestion(testQuestion);
                        
                        }
                    }
                    
                else   {
                         continue;
                        }
                }
            
             return quiz;
        }
        public ArrayList<Question> getQuestions() {
            return this.questions;
        }
        public BorderPane make(ArrayList<Question> question, int order) {
            Quiz quiz = new Quiz();
        BorderPane borderPane = new BorderPane();
        TextArea textArea = new TextArea(order + 1 + ") " + question.get(order).getDescription());
        textArea.setPrefColumnCount(20);
        textArea.setPrefRowCount(5);
        textArea.setWrapText(true);
        textArea.setEditable(false);
        textArea.setStyle("-fx-text-fill: black");
        textArea.setFont(Font.font("Times", 15));
        borderPane.setTop(textArea);
        Button next = new Button("     Next>>");
        Button previous = new Button("<<Previous");
        borderPane.setLeft(previous);
        borderPane.setRight(next);
        
            if (question.get(order) instanceof FillIn) {
              
               
        
            TextField textField = new TextField();
            textField.setEditable(true);
            textField.setFont(Font.font("Times New Roman", 15));
            borderPane.setCenter(textField);
            Label status = new Label(quiz.toString());
            status.setFont(Font.font("Times New Roman", 15));
            
            Button check = new Button("Check Answers");
            HBox hbox = new HBox(150);
            hbox.setPadding(new Insets(5,5,5,50));

            hbox.getChildren().addAll(status, check);
            
            borderPane.setAlignment(hbox, Pos.BOTTOM_CENTER);
            borderPane.setBottom(hbox);
        
            }
            else {
            

               VBox vbox = new VBox(10);
               vbox.setPadding(new Insets(5, 5, 5, 5));
        
    
            
            ToggleGroup group = new ToggleGroup();
            RadioButton radioButton1 = new RadioButton(question.get(order).getOptionAt(0));
            radioButton1.setToggleGroup(group);
            
            RadioButton radioButton2 = new RadioButton(question.get(order).getOptionAt(1));
            radioButton2.setToggleGroup(group);
            
            RadioButton radioButton3 = new RadioButton(question.get(order).getOptionAt(2));
            radioButton3.setToggleGroup(group);
            
            RadioButton radioButton4 = new RadioButton(question.get(order).getOptionAt(3));
            radioButton4.setToggleGroup(group);
            
            VBox box = new VBox(10);
            box.getChildren().addAll(radioButton1, radioButton2, radioButton3, radioButton4);

            
            borderPane.setCenter(box);
            box.setPadding(new Insets(100,5,5,70));
            

            HBox hbox = new HBox(150);
            hbox.setPadding(new Insets(5,5,5,50));



            
            Label status = new Label(quiz.toString());
            status.setFont(Font.font("Times New Roman", 15));
            
            Button check = new Button("Check Answers");

            hbox.getChildren().addAll(status, check);
            
            borderPane.setAlignment(hbox, Pos.BOTTOM_CENTER);
            borderPane.setBottom(hbox);

            }
            return borderPane;

        }
        public void start(ArrayList<Question> question) {
            
        Quiz quiz = new Quiz();

        Stage stage = new Stage();
        
        BorderPane borderPane = new BorderPane();
        TextArea textArea = new TextArea(order + 1 + ") " + question.get(order).getDescription());
        textArea.setPrefColumnCount(20);
        textArea.setPrefRowCount(5);
        textArea.setWrapText(true);
        textArea.setEditable(false);
        textArea.setStyle("-fx-text-fill: black");
        textArea.setFont(Font.font("Times", 15));
        borderPane.setTop(textArea);
        Button next = new Button("     Next>>");
        Button previous = new Button("<<Previous");
        borderPane.setLeft(previous);
        borderPane.setRight(next);
        Label status = new Label(quiz.toString());
            status.setFont(Font.font("Times New Roman", 15));
            
            Button check = new Button("Check Answers");
            HBox hbox = new HBox(150);
            hbox.setPadding(new Insets(5,5,5,50));

            hbox.getChildren().addAll(status, check);
            
            borderPane.setAlignment(hbox, Pos.BOTTOM_CENTER);
            borderPane.setBottom(hbox);
            check.setOnAction(e-> {
                System.out.println("Click");
            });
            
        if (question.get(order) instanceof FillIn) {
              
               
        
            TextField textField = new TextField();
            textField.setEditable(true);
            textField.setFont(Font.font("Times New Roman", 15));
            borderPane.setCenter(textField);
            
        
            }
            else {
            

               VBox vbox = new VBox(10);
               vbox.setPadding(new Insets(5, 5, 5, 5));
        
    
            
            ToggleGroup group = new ToggleGroup();
            RadioButton radioButton1 = new RadioButton(question.get(order).getOptionAt(0));
            radioButton1.setToggleGroup(group);
            
            RadioButton radioButton2 = new RadioButton(question.get(order).getOptionAt(1));
            radioButton2.setToggleGroup(group);
            
            RadioButton radioButton3 = new RadioButton(question.get(order).getOptionAt(2));
            radioButton3.setToggleGroup(group);
            
            RadioButton radioButton4 = new RadioButton(question.get(order).getOptionAt(3));
            radioButton4.setToggleGroup(group);
            
            VBox box = new VBox(10);
            box.getChildren().addAll(radioButton1, radioButton2, radioButton3, radioButton4);

            
            borderPane.setCenter(box);
            box.setPadding(new Insets(100,5,5,70));
            

            }
            
            next.setOnAction(e-> {
                System.out.println(order);
                System.out.println("Clicked");
                Stage stage1 = new Stage();
              order++;
              stage1.setScene(new Scene(make(question, order),500,500));
              stage1.show();
        
        });
        
          
            previous.setOnAction(e-> {
               Stage stage2 = new Stage();
              order--; 
              stage2.setScene(new Scene(make(question, order),500,500));
              stage2.show();
        });
        

              
    
              System.out.println(order);
              Scene scene = new Scene(make(question, order),500,500);
              check.requestFocus();
              next.requestFocus();
              stage.setTitle("Project");
              stage.setScene(scene);
              stage.show();
            
        }
        
    
    public String toString() {
        Quiz quiz = new Quiz();
        return "Status: " + (this.order + 1) + "/5" + " questions.";
        }
}


